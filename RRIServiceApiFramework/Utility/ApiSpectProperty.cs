﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRApiFramework.Utility
{
    public class ApiSpectProperty
    {
        public ApiSpectProperty()
        {
            dataRow = new List<DataTransferProperty>();

        }

        public string programName { get; set; }
        public string endpointName { get; set; }
        public string requestSampleData { get; set; }
        public string responseSampleData { get; set; }
        public string method { get; set; }
        public string sheetName { get; set; }
        public string fileName { get; set; }



        public List<DataTransferProperty> dataRow { get; set; }


    }

    public class DataTransferProperty
    {
        public string input { get; set; } = " ";
        public string layer { get; set; } = " ";
        public string number { get; set; } = " ";
        public string fieldName { get; set; } = " ";
        public string dataType { get; set; } = " ";
        public string mo { get; set; } = " ";
        public string description { get; set; } = "";
        public string sampleData { get; set; } = " ";
        public string remark { get; set; } = " ";


    }
    public class ObjectProperty
    {
        public string propertyTypeName { get; set; }
        public string assamblyName { get; set; }
    }
}
