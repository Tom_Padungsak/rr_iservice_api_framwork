﻿using System.Web.Configuration;

namespace RRApiFramework.Utility
{
    public class ConfigurationUtil
    {
        private static ConfigurationUtil util = new ConfigurationUtil();
        private string sysTemName;
        private string enableRequestObjectOther;
        private string tokenSecretKey;
        private string tokenExpireUnit;
        private int tokenExpireQty;
        private string enableExpireToken;
        private string enableStatusSignOut;
        private string enableSpectAllSystem;
        private string[] linkAllow;
        private string[] linkPermission;
        
        public ConfigurationUtil()
        {

            this.sysTemName = WebConfigurationManager.AppSettings["SYSTEM_NAME"].ToString();
            this.enableRequestObjectOther = WebConfigurationManager.AppSettings["ENABLE_REQUEST_OBJECT_OTHER"].ToString();
            this.tokenSecretKey = WebConfigurationManager.AppSettings["TOKEN_SECRET_KEY"].ToString();
            this.tokenExpireUnit = WebConfigurationManager.AppSettings["TIME_EXPIRE_UNIT"].ToString();
            this.tokenExpireQty = (WebConfigurationManager.AppSettings["TIME_EXPIRE_QTY"].ToString()).ToInt32();
            this.enableExpireToken = WebConfigurationManager.AppSettings["ENABLE_EXPIRE_TOKEN"].ToString();
            this.enableStatusSignOut = WebConfigurationManager.AppSettings["ENABLE_STATUS_SIGNOUT"].ToString();
            string liinkAllowString = WebConfigurationManager.AppSettings["LINK_ALLOW"].ToString();
            this.linkAllow = liinkAllowString.Split(',');
            string liinkPermissionString = WebConfigurationManager.AppSettings["LINK_PERMISSION"].ToString();
            this.linkPermission = liinkPermissionString.Split(',');
            this.enableSpectAllSystem = WebConfigurationManager.AppSettings["ENABLE_SPECT_ALL_SYSTEM"].ToString();
        }
        public static ConfigurationUtil Static
        {
            get { return util = new ConfigurationUtil(); }
        }
        public string SysTemName
        {
            get { return this.sysTemName; }
            set { this.sysTemName = value; }
        }
        public string EnableRequestObjectOther
        {
            get { return this.enableRequestObjectOther; }
            set { this.enableRequestObjectOther = value; }
        }
        public string TokenSecretKey
        {
            get { return this.tokenSecretKey; }
            set { this.tokenSecretKey = value; }
        }
        public string TokenExpireUnit
        {
            get { return this.tokenExpireUnit; }
            set { this.tokenExpireUnit = value; }
        }
        public int TokenExpireQty
        {
            get { return this.tokenExpireQty; }
            set { this.tokenExpireQty = value; }
        }
        public string EnableExpireToken
        {
            get { return this.enableExpireToken; }
            set { this.enableExpireToken = value; }
        }
        public string[] LinkAllow
        {
            get { return this.linkAllow; }
            set { this.linkAllow = value; }
        }
        public string[] LinkPermission
        {
            get { return this.linkPermission; }
            set { this.linkPermission = value; }
        }
        
        public string EnableSpectAllSystem
        {
            get { return this.enableSpectAllSystem; }
            set { this.enableSpectAllSystem = value; }
        }
        public string EnableStatusSignOut
        {
            get { return this.enableStatusSignOut; }
            set { this.enableStatusSignOut = value; }
        }


    }

}