﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using VentilatorModel.Model;

namespace RRApiFramework
{
    public class SaveLog
    {
        public static bool SaveLogObject(object responseMessage, object requestMessage, string ModuleType, string ActionType, DateTime? dateRequest)
        {
            string request = string.Empty;
            string response = JsonConvert.SerializeObject(responseMessage);
            JObject responseJson = JObject.Parse(response);

            if (requestMessage != null)
                request = JsonConvert.SerializeObject(requestMessage);


            var tran_id = responseJson.GetValue("transactionId") == null ? "" : responseJson.GetValue("transactionId").ToString();
            //var req_datetime = responseJson.GetValue("transactionDateTime") != null ? Convert.ToDateTime(responseJson.GetValue("transactionDateTime").ToString()) : DateTime.Now;
            var req_datetime = dateRequest != null ? dateRequest : DateTime.Now;
            var status = responseJson.GetValue("code") != null ? responseJson.GetValue("code").ToString() : "";
            using (var db = new VentilatorVqModelContext())
            {
                LOG_API mylog = new LOG_API
                {
                    TRANSACTION_ID = tran_id,
                    ACTION_TYPE = ActionType,
                    MODULE_TYPE = ModuleType,
                    CREATE_BY = "",
                    CREATE_DATE = DateTime.Now,
                    REQUEST = request,
                    REQUEST_DATETIME = req_datetime,
                    RESPONSE = JsonConvert.SerializeObject(responseMessage),
                    RESPONSE_DATETIME = DateTime.Now,
                    STATUS = status
                };

                db.LOG_API.Add(mylog);
                db.SaveChanges();
            }
            return true;
        }

        public string getIsLogout(string userName)
        {
            var logout = "";
            using (var db = new VentilatorModelContext())
            {
                logout = db.MST_USER.Where(x => x.MUS_IS_ACTIVE == "Y" && x.MUS_USERNAME == userName).Select(x => x.MUS_IS_LOGOUT).FirstOrDefault();
            }
            
            return logout;
        }
    }
}