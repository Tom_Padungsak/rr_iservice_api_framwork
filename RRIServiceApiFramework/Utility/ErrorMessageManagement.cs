﻿using RRApiFramework.HTTP.Response;
using RRApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace RRApiFramework.Utility
{
    public class ErrorMessageManagement
    {
        public string username = "";
        public string password { get; set; }
        public string email { get; set; }
        public string message { get; set; }
        private static ErrorMessageManagement errorMessageUtil;

        public string getErorModelState(ModelStateDictionary modelStateDictionary)
        {
            foreach (var state in modelStateDictionary)
            {
                foreach (var error in state.Value.Errors)
                {
                    this.message += "," + error.ErrorMessage;
                }
            }
            return this.message;
        }
        public static ActionResultStatus GetRequestModelErrorIsValid(ModelStateDictionary modelState)
        {
            List<object> listErr = new List<object>();
            foreach (ModelState vals in modelState.Values)
            {
                foreach (var item in vals.Errors)
                {
                    if (!item.ErrorMessage.IsNullOrEmptyWhiteSpace())
                        listErr.Add(item.ErrorMessage);
                    else
                        listErr.Add(item.Exception.Message);
                }
            }

            ActionResultStatus actionResultStatus = new ActionResultStatus
            {
                success = false,
                code = (int)StatusCode.InvalidData,
                data = listErr,
                message = StatusCode.InvalidData.Value(),
                description = StatusCode.InvalidData.Value()
            };

            return actionResultStatus;
        }

        public static ErrorMessageManagement ErrorMessageUtil
        {
            get { return errorMessageUtil = new ErrorMessageManagement(); }
        }

    }


}