﻿using RRApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RRApiFramework.Utility
{
    public static class GetAttributeProperty
    {
        public static T GetAttribute<T>(this MemberInfo member, bool isRequired)
            where T : Attribute
        {
            var attribute = member.GetCustomAttributes(typeof(T), false).SingleOrDefault();

            if (attribute == null && isRequired)
            {
                throw new ArgumentException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "The {0} attribute must be defined on member {1}",
                        typeof(T).Name,
                        member.Name));
            }

            return (T)attribute;
        }

        public static string GetPropertyDisplayName<T>(Expression<Func<T, object>> propertyExpression)
        {
            var memberInfo = GetPropertyInformation(propertyExpression.Body);
            if (memberInfo == null)
            {
                throw new ArgumentException(
                    "No property reference expression was found.",
                    "propertyExpression");
            }

            var attr = memberInfo.GetAttribute<DisplayNameAttribute>(false);
            if (attr == null)
            {
                return memberInfo.Name;
            }

            return attr.DisplayName;
        }

        public static string GetPropertyDescription<T>(Expression<Func<T, object>> propertyExpression)
        {
            var memberInfo = GetPropertyInformation(propertyExpression.Body);
            if (memberInfo == null)
            {
                throw new ArgumentException(
                    "No property reference expression was found.",
                    "propertyExpression");
            }

            var attr = memberInfo.GetAttribute<DescriptionAttribute>(false);
            if (attr == null)
            {
                return memberInfo.Name;
            }

            return attr.Description;
        }

        public static MemberInfo GetPropertyInformation(Expression propertyExpression)
        {
            Debug.Assert(propertyExpression != null, "propertyExpression != null");
            MemberExpression memberExpr = propertyExpression as MemberExpression;
            if (memberExpr == null)
            {
                UnaryExpression unaryExpr = propertyExpression as UnaryExpression;
                if (unaryExpr != null && unaryExpr.NodeType == ExpressionType.Convert)
                {
                    memberExpr = unaryExpr.Operand as MemberExpression;
                }
            }

            if (memberExpr != null && memberExpr.Member.MemberType == MemberTypes.Property)
            {
                return memberExpr.Member;
            }

            return null;
        }
        public static string GetPropertyTypeFromObjoct(Type type, string fileName, ComponentModelType propertyType)
        {
            string result = string.Empty;
            MemberInfo property = type.GetProperty(fileName);

            if (propertyType == ComponentModelType.Description)
            {
                DescriptionAttribute displayAttribute = new DescriptionAttribute();
                displayAttribute = property.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (displayAttribute != null)
                {
                    result = displayAttribute.Description;
                }
            }

            if (propertyType == ComponentModelType.DisplayName)
            {
                DisplayNameAttribute displayNameAttribute = new DisplayNameAttribute();
                displayNameAttribute = property.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                if (displayNameAttribute != null)
                {
                    result = displayNameAttribute.DisplayName;
                }
            }

            if (propertyType == ComponentModelType.Required)
            {
                RequiredAttribute requiredAttribute = new RequiredAttribute();
                requiredAttribute = property.GetCustomAttribute(typeof(RequiredAttribute)) as RequiredAttribute;
                if (requiredAttribute != null)
                {
                    result = requiredAttribute.TypeId.ToString();
                }
            }

            if (propertyType == ComponentModelType.MaxLength)
            {
                MaxLengthAttribute maxLengthAttribute = new MaxLengthAttribute();
                maxLengthAttribute = property.GetCustomAttribute(typeof(MaxLengthAttribute)) as MaxLengthAttribute;
                if (maxLengthAttribute != null)
                {
                    result = maxLengthAttribute.Length.ToString();
                }
            }

            return result;
        }
    }

}
