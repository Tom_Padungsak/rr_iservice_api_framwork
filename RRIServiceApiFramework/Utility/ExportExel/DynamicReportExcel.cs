﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RRApiFramework.Utility.ExportExel
{
    public static class DynamicReportExcel
    {
        public static void ExportSpectApi(ApiSpectProperty req)
        {


            SummaryPaymentRequest summaryPaymentRequest = new SummaryPaymentRequest();

            summaryPaymentRequest.sheetName = req.sheetName;

            List<DynamicReportHeaderModel> headerList = new List<DynamicReportHeaderModel>();
            DynamicReportHeaderModel header1 = new DynamicReportHeaderModel()
            {
                row = 1,
                col = 1,
                headerLabel = "System Name",
                headerValue = null,
                style = new Style()
            };

            DynamicReportHeaderModel header2 = new DynamicReportHeaderModel()
            {
                row = 1,
                col = 2,
                headerLabel = req.programName,
                headerValue = null,
                style = new Style()
            };

            DynamicReportHeaderModel header3 = new DynamicReportHeaderModel()
            {
                row = 2,
                col = 1,
                headerLabel = "Endpoint",
                headerValue = null,
                style = new Style()
            };

            DynamicReportHeaderModel header4 = new DynamicReportHeaderModel()
            {
                row = 2,
                col = 2,
                headerLabel = req.endpointName,
                headerValue = null,
                style = new Style()
               
            };

            DynamicReportHeaderModel header5 = new DynamicReportHeaderModel()
            {
                row = 3,
                col = 1,
                headerLabel = "Method",
                headerValue = null,
                style = new Style()
            };

            DynamicReportHeaderModel header6 = new DynamicReportHeaderModel()
            {
                row = 3,
                col = 2,
                headerLabel = req.method,
                headerValue = null,
                style = new Style()
               
            };


            DynamicReportHeaderModel header7 = new DynamicReportHeaderModel()
            {
                row = 4,
                col = 2,
                headerLabel = "M = Mandatory / Required , O = Optional / Not Required",
                headerValue = null,
                style = new Style()
                {
                    //cellMerge = "B2:F2",
                    fontColor = "#F10E0E",
                    textRatation = 1,
                    cellMergeQuantity = new CellProperty()
                    {
                        col = 6, row = 1
                    },
                    isMerge = true
                }
            };
            summaryPaymentRequest.reportHeader = new List<DynamicReportHeaderModel>();
            summaryPaymentRequest.reportHeader.Add(header1);
            summaryPaymentRequest.reportHeader.Add(header2);
            summaryPaymentRequest.reportHeader.Add(header3);
            summaryPaymentRequest.reportHeader.Add(header4);
            summaryPaymentRequest.reportHeader.Add(header5);
            summaryPaymentRequest.reportHeader.Add(header6);
            summaryPaymentRequest.reportHeader.Add(header7);

            //////////////////////////////////////////////

            summaryPaymentRequest.reportColumn = new List<DynamicReportColumnModel>();

            //public string layer { get; set; }
            //public string number { get; set; }
            //public string fieldName { get; set; }
            //public string dataType { get; set; }
            //public string description { get; set; }
            //public string sampleData { get; set; }
            //public string remark { get; set; }
            // Input / Output


            //Input / Output
            DynamicReportColumnModel tableCollumn1 = new DynamicReportColumnModel()
            {
                dataIndex = "input",
                title = "Input / Output",
                dataType = "S",
                style = new Style()
            };

            DynamicReportColumnModel tableCollumn2 = new DynamicReportColumnModel()
            {
                dataIndex = "layer",
                title = "Layer",
                dataType = "S",
                style = new Style()
            };

            DynamicReportColumnModel tableCollumn3 = new DynamicReportColumnModel()
            {
                dataIndex = "number",
                title = "No",
                dataType = "S",
                style = new Style()
            };

            DynamicReportColumnModel tableCollumn4 = new DynamicReportColumnModel()
            {
                dataIndex = "fieldName",
                title = "Field Name",
                dataType = "S",
                style = new Style()
            };

            DynamicReportColumnModel tableCollumn5 = new DynamicReportColumnModel()
            {
                dataIndex = "dataType",
                title = "Data Type",
                dataType = "S",
                style = new Style()
            };


            DynamicReportColumnModel tableCollumn6 = new DynamicReportColumnModel()
            {
                dataIndex = "mo",
                title = "M/O",
                dataType = "S",
                style = new Style()
            };

            DynamicReportColumnModel tableCollumn7 = new DynamicReportColumnModel()
            {
                dataIndex = "description",
                title = "Description",
                dataType = "S",
                style = new Style()
            };

            DynamicReportColumnModel tableCollumn8 = new DynamicReportColumnModel()
            {
                dataIndex = "remark",
                title = "Remark",
                dataType = "S",
                style = new Style()
            };


            summaryPaymentRequest.reportColumn = new List<DynamicReportColumnModel>();
            summaryPaymentRequest.reportColumn.Add(tableCollumn1);
            summaryPaymentRequest.reportColumn.Add(tableCollumn2);
            summaryPaymentRequest.reportColumn.Add(tableCollumn3);
            summaryPaymentRequest.reportColumn.Add(tableCollumn4);
            summaryPaymentRequest.reportColumn.Add(tableCollumn5);
            summaryPaymentRequest.reportColumn.Add(tableCollumn6);
            summaryPaymentRequest.reportColumn.Add(tableCollumn7);
            summaryPaymentRequest.reportColumn.Add(tableCollumn8);

            summaryPaymentRequest.reportData = req.dataRow;

            summaryPaymentRequest.subReportColumn = new List<DynamicReportColumnModel>();

            DynamicReportColumnModel subReportColumn1 = new DynamicReportColumnModel()
            {
                dataIndex = "description",
                title = "Description",
                dataType = "S",
                style = new Style()
            };

            DynamicReportColumnModel subReportColumn2 = new DynamicReportColumnModel()
            {
                dataIndex = "remark",
                title = "Remark",
                dataType = "S",
                style = new Style()
            };



            DynamicReportHeaderModel footer1 = new DynamicReportHeaderModel()
            {
                col = 1,
                row = 1,
                headerLabel = "Request",
                headerValue = null,
                style = new Style()
                {
                    fontSize = 8,
                    backgroundColor = "#0C0201",
                    fontColor = "#FFFFFF",
                    textAlignHorizontal = "C",
                    border = true,
            cellMergeQuantity = new CellProperty()
                    {
                        col = 6,
                        row = 1
                    },
                    isMerge = true
                }
            };

            DynamicReportHeaderModel footer2 = new DynamicReportHeaderModel()
            {
                col = 2,
                row = 1,
                headerLabel = "Response",
                headerValue = null,

                style = new Style()
                {
                    fontSize = 8,
                    fontColor = "#FFFFFF",
                    backgroundColor = "#0C0201",
                    textAlignHorizontal = "C",
                    border = true,
                    cellMergeQuantity = new CellProperty()
                    {
                        col = 6,
                        row = 1
                    },
                    isMerge = true
                }
            };

            DynamicReportHeaderModel footer3 = new DynamicReportHeaderModel()
            {
                col = 1,
                row = 2,
                headerLabel = req.requestSampleData,
                headerValue = null,
                style = new Style()
                {
                    fontSize = 8,
                    fontColor = "#0C0201",
                    backgroundColor = "#FFFFFF",
                    textAlignHorizontal = "L",
                    textAlignVertical = "T",
                    border = true,
                    cellMergeQuantity = new CellProperty()
                    {
                        col = 6,
                        row = 30
                    },
                    isMerge = true
                }
            };

            DynamicReportHeaderModel footer4 = new DynamicReportHeaderModel()
            {
                col = 2,
                row = 2,
                headerLabel = req.responseSampleData,
                headerValue = null,
                
                style = new Style()
                {
                    fontSize = 8,
                    fontColor = "#0C0201",
                    backgroundColor = "#FFFFFF",
                    textAlignHorizontal = "L",
                    textAlignVertical = "T",
                    border = true,
                    cellMergeQuantity = new CellProperty()
                    {
                        col = 6,
                        row = 30
                    },
                    isMerge = true
                }

            };
            summaryPaymentRequest.reportFooter = new List<DynamicReportHeaderModel>();
            summaryPaymentRequest.reportFooter.Add(footer1);
            summaryPaymentRequest.reportFooter.Add(footer2);
            summaryPaymentRequest.reportFooter.Add(footer3);
            summaryPaymentRequest.reportFooter.Add(footer4);





            Export(summaryPaymentRequest);
        }


        public static void Export(SummaryPaymentRequest items)
        {
            Excel excel = new Excel();
            XLWorkbook workbook = new XLWorkbook();
            int sheet = 1;
         
                var ws = workbook.Worksheets.Add(string.IsNullOrEmpty(items.sheetName) ? $"{"sheet"}{sheet}" : items.sheetName);

                #region Create Header
                var headerRow = 1;
                var labelSeparate = "";
                foreach (DynamicReportHeaderModel item in items.reportHeader)
                {
                    labelSeparate = !string.IsNullOrEmpty(item.headerValue) ? ": " : "";
                    RenderHeaderSection(ws, headerRow, labelSeparate, item);
                    excel.setStyle(ws, (item.col != null && item.row != null ? item.row.Value : headerRow), (item.col != null && item.row != null ? item.col.Value : 1), item.style);
                    headerRow++;
                }
                excel.setHeaderExcel(ws);
                #endregion

                #region  Create Text title Table
                string[] reportColumnNameList = items.reportColumn.Select(a => a.title).ToArray();
                int row = ws.LastRowUsed().RowNumber() + 2;

                if (items.otherData != null)
                {
                    if (!string.IsNullOrEmpty(items.otherData.titleTable))
                    {
                        ws.Cell(row, 1).Value = items.otherData.titleTable;
                        row++;
                    }
                }

                var columnIndex = 1;
                foreach (var item in items.reportColumn)
                {
                    //var aa = ws.Cell((item.col != null && item.row != null ? item.row.Value : row), (item.col != null && item.row != null ? item.col.Value : columnIndex));
                    ws.Cell((item.col != null && item.row != null ? item.row.Value : row), (item.col != null && item.row != null ? item.col.Value : columnIndex)).Value = item.title;
                    excel.setStyle(ws, (item.col != null && item.row != null ? item.row.Value : row), (item.col != null && item.row != null ? item.col.Value : columnIndex), item.style);
                    //excel.setHeaderTableExcel(ws);
                    columnIndex++;
                }
                excel.setHeaderTableExcel(ws);
                #endregion

                //#region First Data Table
                string[] reportColumnCodeList = items.reportColumn.Select(a => a.dataIndex).ToArray();
                row++;
                if (items.isSummary)
                {
                    columnIndex = 1;
                    foreach (var item in items.summaryData)
                    {
                        excel.setStyle(ws, item.row.Value, item.col.Value, item.style);
                        RenderDataTableSection(excel, ws, item.row.Value, item.col.Value, item.style.dataType, item.title);
                        //columnIndex++;
                    }
                }
                else
                {
                    dynamic dataDynamic = items.reportData;
                    foreach (dynamic item in dataDynamic)
                    {
                        columnIndex = 1;
                        foreach (var col in items.reportColumn)
                        {
                            Type ItemType = item.GetType();
                            IList<PropertyInfo> props = new List<PropertyInfo>(ItemType.GetProperties());
                            var data = props.Where(r => r.Name == col.dataIndex).FirstOrDefault();
                            if (data != null)
                            {
                                ws.Cell(row, columnIndex).Style.NumberFormat.Format = "@";
                                string propValue = data.GetValue(item, null).ToString();
                                RenderDataTableSection(excel, ws, row, columnIndex, col.dataType, propValue);
                                columnIndex++;
                            }
                        }
                        row++;
                    }
                    excel.setFormatExcel(ws);
                }


            #region Create Footter
            int rowAll = row;
            var footerLabelSeparate = "";
            int currentRow = 0;
            int lengthCollumnBefore = 0;
            int rowDataCurrent = 0;
            int rowDataBefore = 0;
            foreach (DynamicReportHeaderModel item in items.reportFooter)
            {
                rowDataCurrent = (int)item.row;
                if(rowDataBefore == 0)
                    rowDataBefore = (int)item.row;


                int footterRow = rowAll + (int)item.row;
                footerLabelSeparate = !string.IsNullOrEmpty(item.headerValue) ? ": " : "";
                
                if (rowDataCurrent == rowDataBefore)
                {
                    if (item.style.isMerge)
                    {
                        item.col = lengthCollumnBefore + 1;
                        lengthCollumnBefore = item.style.cellMergeQuantity.col;
                    }
                }
                else
                {
                    lengthCollumnBefore = 0;
                    lengthCollumnBefore = lengthCollumnBefore + item.style.cellMergeQuantity.col;

                }

                item.row = footterRow;
                RenderHeaderSection(ws, footterRow, footerLabelSeparate, item);
                excel.setStyle(ws, footterRow, (int)item.col, item.style);
                currentRow = footterRow;
                rowDataBefore = rowDataCurrent;

            }
            excel.setFooterExcel(ws);
            #endregion

         
            int lastColumnNumber = ws.LastColumnUsed().ColumnNumber();
            for (int i = 1; i <= lastColumnNumber; i++)
            {
                ws.Column(i).AdjustToContents();
            }
            sheet++;

            Exports.Excel(workbook, items.sheetName);
        }

        private static void RenderHeaderSection(IXLWorksheet ws, int headerRow, string labelSeparate, DynamicReportHeaderModel item)
        {
            if (item.col != null && item.row != null)
            {
                ws.Cell(item.row.Value, item.col.Value).Value = $"{item.headerLabel}{labelSeparate}{item.headerValue}";
            }
            else
            {
                ws.Cell(headerRow, 1).Value = $"{item.headerLabel}{labelSeparate}{item.headerValue}";
            }

        }

        private static void RenderDataTableSection(Excel excel, IXLWorksheet ws, int row, int col, string dataType, string value)
        {
            ws.Cell(row, col).Value = value;
            excel.setDataTypeFormat(ws, row, col, dataType, value);
        }
    }
}