﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRApiFramework.Utility.ExportExel
{
    public class DynamicReportRequest
    {
        public List<DynamicReportHeaderModel> reportHeader { get; set; }
        public List<DynamicReportColumnModel> reportColumn { get; set; }
        public List<DynamicReportHeaderModel> subReportHeader { get; set; }
        public List<DynamicReportColumnModel> subReportColumn { get; set; }
        public List<DynamicReportHeaderModel> reportFooter { get; set; }

        public List<DynamicReportDataTableModel> dataTable { get; set; }
        public string sheetName { get; set; }
        public Style style { get; set; }
    }

    public class DynamicReportHeaderModel
    {
        public string headerLabel { get; set; }
        public string headerValue { get; set; }
        public int? row { get; set; }
        public int? col { get; set; }
        public Style style { get; set; }
        //public string textAlign { get; set; } = "L";

    }

    public class DynamicReportColumnModel
    {
        public int? row { get; set; }
        public int? col { get; set; }
        public string dataIndex { get; set; }
        public string title { get; set; }
        public string dataType { get; set; } = "S";
        public Style style { get; set; }
    }

    public class DynamicReportDataTableModel
    {
        public string value { get; set; }
        public int? row { get; set; }
        public int? col { get; set; }
        public Style style { get; set; }
    }

    public class OtherData
    {
        public double? sumTotal { get; set; }
        public double? butgetCost { get; set; }
        public string titleTable { get; set; }
        public bool showGridLines { get; set; } = true;
        public Style style { get; set; }
    }

    public class Style : MergeCell
    {
        public Style()
        {
            fontName = "Tahoma";
            fontBold = false;
            fontSize = 8;
            showGridLines = true;
            underLineId = 2;
            textAlignVertical = "B";
            textAlignHorizontal = "L";
            textRatation = 0;
            dataType = "S";
            border = false;
            backgroundColor = "";
            fontColor = "";
        }
        public string fontName { get; set; }
        public bool fontBold { get; set; }
        public int fontSize { get; set; }
        public bool showGridLines { get; set; }
        public string textAlignHorizontal { get; set; }
        public string textAlignVertical { get; set; }
        public int underLineId { get; set; }
        public int textRatation { get; set; }
        /// <summary>
        /// Default String. parameter S = String, N = Numeric, D = Date, DT = DateTime
        /// </summary>
        public string dataType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string backgroundColor { get; set; }
        public string fontColor { get; set; }
        public bool border { get; set; }
    }
    public class MergeCell
    {
        public bool isMerge { get; set; }
        public string cellMerge { get; set; }
        public CellProperty cellMergeQuantity { get; set; }
    }

    public enum UnderLine
    {
        Double = 0,
        DoubleAccounting = 1,
        None = 2,
        Single = 3,
        SingleAccounting = 4
    }
}