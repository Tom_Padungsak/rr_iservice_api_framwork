﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace RRApiFramework.Utility
{
    public class DateFormat
    {
        public static string ToSystemString(DateTime? dateTime)
        {
            string format = WebConfigurationManager.AppSettings["DateTimeFormat"];
            return dateTime == null ? "" : Convert.ToDateTime(dateTime).ToString(format);
        }
    }
}