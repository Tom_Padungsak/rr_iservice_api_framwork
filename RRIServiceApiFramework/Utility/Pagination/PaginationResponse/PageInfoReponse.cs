﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRApiFramework.Utility.Pagination
{
    public class PageInfoReponse
    {
        public int totalResults { get; set; }
        public int resultsPerPage { get; set; }

        public PageInfoReponse()
        {
            totalResults = 0;
            resultsPerPage = 0;
        }
    }
}