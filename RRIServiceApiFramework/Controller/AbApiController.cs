﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRApiFramework.Utility.ExportExel;
using RRApiFramework.Utility.Pagination;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using RRApiFramework.Model.Enum;
using System.Security.Claims;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Net.Http;

namespace RRApiFramework.Controller
{
    public abstract class AbApiController : ApiController
    {
        private dynamic itemData;
        private dynamic otherItemData;
        private dynamic filterData;
        private object sortData;
        private dynamic inputRequest;
        private dynamic responseResult;
        private PaginationRequest inputRequestPaging;
        private bool enblePaging;
        private bool enbleDataList;
        private string methodName;
        private string moduleName;
        private string method;
        private bool enableSaveRequestBeforMapper;
        private bool enableResponseApiSpect;
        private bool enbleModelStateRequest;
        private ActionResultStatus resultStatus;
        private ApiSpectProperty apiSpectProperty;
        private UserAccess userAccess;
        private bool enableSaveFullRequest;
        private bool enbleSaveLog;
        private string tokenApi;
        private string apiKey;
        public DateTime dateRequest;

        public AbApiController()
        {
            
            this.enblePaging = false;
            this.enbleDataList = false;
            this.enableResponseApiSpect = false;
            this.enableSaveRequestBeforMapper = false;
            this.method = HttpContext.Current.Request.RequestType;
            this.methodName = HttpContext.Current.Request.Path;
            this.enbleModelStateRequest = false;
            this.enableSaveFullRequest = false;
            this.enbleSaveLog = true;
            this.apiSpectProperty = new ApiSpectProperty();
            this.dateRequest = DateTime.Now;
            this.tokenApi = JwtManagement.GenerateJasonWebToken(this.userAccess);

            var claimsIdentity = User.Identity as ClaimsIdentity;
            string userAccessString = claimsIdentity?.Name;

            if (!userAccessString.IsNullOrEmptyWhiteSpace())
                this.userAccess = JsonConvert.DeserializeObject<UserAccess>(userAccessString);
        }

        public static string RequestBody()
        {
            var bodyStream = new StreamReader(HttpContext.Current.Request.InputStream);
            bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();
            return bodyText;
        }
      
        public abstract string ModuleName { get; }

        public string MethodName
        {
            get { return this.methodName; }
            set { this.methodName = value; }
        }

        public string TokenApi
        {
            get { return this.tokenApi; }
            set { this.tokenApi = value; }
        }

        public bool EnblePaging
        {
            get { return this.enblePaging; }
            set { this.enblePaging = value; }
        }
        public bool EnbleDataList
        {
            get { return this.enbleDataList; }
            set { this.enbleDataList = value; }
        }

        private static string GetEntityErrorException(Exception exception)
        {
            string resultException = string.Empty;

            var entityException = exception.GetType();
            if (entityException == typeof(DbEntityValidationException))
            {
                resultException += EntityErrorException(exception);
            }
            else
            {
                if (exception.InnerException != null)
                {
                    if (exception.InnerException.InnerException != null)
                    {
                        if (exception.InnerException.InnerException.InnerException != null)
                            resultException += exception.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                        else
                            resultException += exception.InnerException.InnerException.Message + Environment.NewLine;
                    }
                    else
                    {
                        resultException += exception.InnerException.Message + Environment.NewLine;
                    }
                }
                else
                {
                    resultException += exception.Message + Environment.NewLine;
                }
            }
            return resultException;
        }
        private static string EntityErrorException(Exception exception)
        {
            string result = string.Empty;
            var errorResult = string.Empty;
            var counts = 1;
            var entityErrors = (DbEntityValidationException)exception;
            foreach (var error in entityErrors.EntityValidationErrors)
            {
                foreach (var err in error.ValidationErrors)
                {
                    if (errorResult.Length > 0)
                        errorResult += Environment.NewLine;

                    errorResult += string.Format("{0}. Error Message : {2}",
                        counts, err.PropertyName, err.ErrorMessage);
                    counts++;
                }
            }

            return errorResult + Environment.NewLine;
        }

        public virtual bool InputRequest(object inputRequest)
        {
            bool result = false;
            this.inputRequest = inputRequest;
            if (this.inputRequest != null)
                result = true;

            return result;
        }
        public virtual bool SetFilter(List<object> filterData)
        {
            bool result = false;
            this.filterData = filterData;
            if (this.filterData != null)
                result = true;

            return result;
        }

        public virtual bool SetSorting(object sortData)
        {
            bool result = false;
            this.sortData = sortData;
            if (this.sortData != null)
                result = true;

            return result;
        }

        public bool EnableResponseApiSpect
        {
            get { return this.enableResponseApiSpect; }
            set { this.enableResponseApiSpect = value; }
        }
        public bool EnableSaveRequestBeforMapper
        {
            get { return this.enableSaveRequestBeforMapper; }
            set { this.enableSaveRequestBeforMapper = value; }
        }
        public bool EnbleModelStateRequest
        {
            get { return this.enbleModelStateRequest; }
            set { this.enbleModelStateRequest = value; }
        }
        public UserAccess UserAccessApi
        {
            get { return this.userAccess; }
            set { this.userAccess = value; }
        }
        public bool EnableSaveFullRequest
        {
            get { return this.enableSaveFullRequest; }
            set { this.enableSaveFullRequest = value; }
        }
        public bool EnbleSaveLog
        {
            get { return this.enbleSaveLog; }
            set { this.enbleSaveLog = value; }
        }
        public bool SetApiKey()
        {
            bool result = false;
            var dataPermission = this.userAccess.apiKeyList.Find(ele => ConfigurationUtil.Static.LinkPermission.Contains(ele));
            if (dataPermission != null || ConfigurationUtil.Static.LinkPermission.Length ==0)
            {
                result = true;
            }
            else
            {
                ActionResultStatus actionResultStatus = new ActionResultStatus();
                actionResultStatus.code = (int)HttpStatusCode.Unauthorized;
                actionResultStatus.data = null;
                actionResultStatus.message = "Token has authrize api key AA";
                actionResultStatus.description = null;
                result = false;
                this.SendResponse(actionResultStatus);
            }

            return result;
        }
    

        public virtual IHttpActionResult SendResponse(ActionResultStatus actionResultStatus)
        {
            APIResultResponseListPaging apiResultListPaging = new APIResultResponseListPaging();
            APIResultResponse apiResultList = new APIResultResponse();
            dynamic responseData;
            try
            {
                if (actionResultStatus.code.Equals((int)HttpStatusCode.Unauthorized))
                {
                    System.Net.Http.HttpResponseMessage response;
                    response = this.Request.CreateResponse(HttpStatusCode.Unauthorized);
                    response.Headers.Add("success", "true");
                    response.Headers.Add("code", actionResultStatus.code.ToString());
                    response.Headers.Add("message", actionResultStatus.message);

                    return ResponseMessage(response);
                }
               

                if (this.enableSaveRequestBeforMapper)
                {
                    var requestBody = JsonConvert.DeserializeObject<object>(RequestBody());
                    if (this.method == "POST")
                        SaveLog.SaveLogObject(this.inputRequest, JObject.Parse(JsonConvert.SerializeObject(requestBody)), "SaveRequest", methodName, this.dateRequest);//save log output
                }
                if (this.enableSaveFullRequest)
                {
                    string request = "Path :" + HttpContext.Current.Request.Path + " , FilePath : " + HttpContext.Current.Request.FilePath + " ,RawUrl : " + HttpContext.Current.Request.RawUrl;
                    if (this.method == "POST")
                        SaveLog.SaveLogObject(this.inputRequest, request, "SaveFullRequest", methodName, this.dateRequest);//save log output
                }

                if (this.enbleModelStateRequest)
                {
                    if (!ModelState.IsValid)
                    {
                        actionResultStatus = ErrorMessage.GetRequestModelError(ModelState);

                        apiResultList.success = actionResultStatus.success;
<<<<<<< HEAD
                        apiResultList.key = actionResultStatus.key.IsNullOrEmptyWhiteSpace() ? JwtManagement.GenerateJasonWebToken(this.userAccess) : actionResultStatus.key;
=======
                        if (actionResultStatus.success == true)
                            apiResultList.key = actionResultStatus.key.IsNullOrEmptyWhiteSpace() ? JwtManagement.GenerateJasonWebToken(this.userAccess) : actionResultStatus.key;
>>>>>>> c7cd5d6896efbc6982e3ae9800f6e451203eadf0
                        apiResultList.code = actionResultStatus.code.ToString();
                        apiResultList.message = actionResultStatus.message;
                        apiResultList.items = actionResultStatus.data;
                        apiResultList.description = actionResultStatus.description;

                        this.responseResult = HttpManageResponse.Response.ResponseResultList(this.Request, apiResultList, ModelState, 
                            this.inputRequest, ModuleName, methodName,this.enbleSaveLog, this.dateRequest);
                        return ResponseMessage(this.responseResult);
                    }
                }

                if (this.enblePaging)
                {
                    if (this.filterData == null)
                        this.filterData = new List<object>();

                    apiResultListPaging.success = actionResultStatus.success;
                    apiResultListPaging.code = actionResultStatus.code.ToString();
                    apiResultListPaging.message = actionResultStatus.message;
                    apiResultListPaging.description = actionResultStatus.description;
<<<<<<< HEAD
                    apiResultListPaging.key = actionResultStatus.key.IsNullOrEmptyWhiteSpace() ? JwtManagement.GenerateJasonWebToken(this.userAccess) : actionResultStatus.key;
=======
                    if(actionResultStatus.success == true)
                        apiResultListPaging.key = actionResultStatus.key.IsNullOrEmptyWhiteSpace() ? JwtManagement.GenerateJasonWebToken(this.userAccess) : actionResultStatus.key;
>>>>>>> c7cd5d6896efbc6982e3ae9800f6e451203eadf0

                    PropertyInfo pagination = this.inputRequest.GetType().GetProperty("pagination");
                    this.inputRequestPaging = (PaginationRequest)(pagination.GetValue(this.inputRequest, null));

                    if (this.inputRequestPaging == null)
                    {
                        apiResultList.success = false;
                        apiResultList.code = actionResultStatus.code.ToString();
                        apiResultList.message = "Can not inherit PaginiationMaster on class request";
                        apiResultList.description = actionResultStatus.description;
                    }


                    if (actionResultStatus.filter != null)
                        apiResultListPaging.filter = actionResultStatus.filter;

                    if (actionResultStatus.data != null)
                        apiResultListPaging.items = actionResultStatus.data;

                    if (actionResultStatus.otherData != null)
                        apiResultListPaging.otherItems = actionResultStatus.otherData;
                    else
                    {
                        if (ConfigurationUtil.Static.EnableRequestObjectOther == StatusYesNo.Yes.Value())
                            apiResultListPaging.otherItems = this.inputRequest;
                    }

                    List<object> dataPage = new List<object>();
                    dataPage.AddRange(Enumerable.ToList<object>(actionResultStatus.data));
                    responseData = apiResultListPaging;
                    this.responseResult = apiResultListPaging.ResponseResult(this.Request, dataPage, ModelState, this.inputRequestPaging, this.inputRequest,
                        this.moduleName, this.methodName, this.enbleSaveLog);
                }
                else
                {

                    apiResultList.success = actionResultStatus.success;

                    if (actionResultStatus.success == true)
                        apiResultList.key = actionResultStatus.key.IsNullOrEmptyWhiteSpace() ? JwtManagement.GenerateJasonWebToken(this.userAccess) : actionResultStatus.key;
                    apiResultList.code = actionResultStatus.code.ToString();
                    apiResultList.message = actionResultStatus.message;
                    apiResultList.description = actionResultStatus.description;

                    if (actionResultStatus.data != null)
                        apiResultList.items = actionResultStatus.data;

                    if (actionResultStatus.otherData != null)
                        apiResultList.otherItems = actionResultStatus.otherData;

                    if (actionResultStatus.filter != null)
                        apiResultList.filter = actionResultStatus.filter;

                    responseData = apiResultList;
                    this.responseResult = HttpManageResponse.Response.ResponseResultList(this.Request, apiResultList,ModelState, 
                        this.inputRequest, ModuleName, methodName,this.enbleSaveLog, this.dateRequest);
                  
                }

                if (this.enableResponseApiSpect || ConfigurationUtil.Static.EnableSpectAllSystem == StatusYesNo.Yes.Value())
                {
                    this.apiSpectProperty.endpointName = HttpContext.Current.Request.RawUrl;
                    this.apiSpectProperty.programName = ConfigurationUtil.Static.SysTemName;
                    this.apiSpectProperty.method = this.method;
                    this.apiSpectProperty.sheetName = this.methodName;
                    this.apiSpectProperty.dataRow.Add(GetHeaderPropertySpect("Header"));
                    this.apiSpectProperty.dataRow.Add(new DataTransferProperty());

                    this.itemData = actionResultStatus.data;
                    this.otherItemData = actionResultStatus.otherData;
                    this.filterData = actionResultStatus.filter;

                    if (this.method == Model.Enum.HttpMethod.Post.Value())
                    {
                        if (this.inputRequest != null)
                        {
                            this.apiSpectProperty.requestSampleData = JsonConvert.SerializeObject(this.inputRequest, Formatting.Indented);
                            this.apiSpectProperty.dataRow.AddRange(GetMasterPropertySpect(this.inputRequest, "Request"));
                            this.apiSpectProperty.dataRow.Add(new DataTransferProperty());
                        }
                        this.apiSpectProperty.dataRow.AddRange(GetMasterPropertySpect(responseData, "Response"));
                    }

                    this.apiSpectProperty.responseSampleData = JsonConvert.SerializeObject(responseData, Formatting.Indented);
                    DynamicReportExcel.ExportSpectApi(this.apiSpectProperty);
                }

             

            }
            catch (Exception ex)
            {
                if (this.enblePaging)
                {
                    var response = HttpManageResponse.Response.ResponseMessage(this.Request, Model.Enum.StatusCode.EROR, ex.Message.ToString(), apiResultListPaging);
                    SaveLog.SaveLogObject(apiResultListPaging, this.inputRequest, ModuleName, methodName, this.dateRequest);//save log output
                    return ResponseMessage(response);
                }
                else
                {
                    var response = HttpManageResponse.Response.ResponseMessage(this.Request, Model.Enum.StatusCode.EROR, ex.Message.ToString(), apiResultList);
                    SaveLog.SaveLogObject(apiResultList, this.inputRequest, ModuleName, methodName, this.dateRequest);//save log output
                    return ResponseMessage(response);
                }


            }

            return ResponseMessage(responseResult);

        }

        public List<DataTransferProperty> GetMasterPropertySpect(dynamic inputData, string sendType)
        {
            List<DataTransferProperty> result = new List<DataTransferProperty>();
            Type requestType = inputData.GetType();
            IList<PropertyInfo> requestProps = new List<PropertyInfo>(requestType.GetProperties());
            int runnungNumber = 1;
            string inputCollumnNew = string.Empty;
            string inputCollumnLast = string.Empty;

            foreach (var requestProp in requestProps)
            {
                inputCollumnNew = sendType;
                DataTransferProperty propertyData = new DataTransferProperty();
                propertyData.mo = !string.IsNullOrWhiteSpace(GetAttributeProperty.GetPropertyTypeFromObjoct(requestType, requestProp.Name, ComponentModelType.Required)) ? RequireDataType.Mandatory.Value() : RequireDataType.Optional.Value();
                propertyData.description = GetAttributeProperty.GetPropertyTypeFromObjoct(requestType, requestProp.Name, ComponentModelType.Description);
                propertyData.fieldName = requestProp.Name;
                propertyData.number = runnungNumber.ToString();
                propertyData.dataType = requestProp.PropertyType.Name;

                if (inputCollumnNew != inputCollumnLast)
                    propertyData.input = sendType;

                if (requestProp.PropertyType.Equals(typeof(string)))
                {
                    propertyData.dataType = SpecApiDataType.String.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(List<string>)))
                {
                    propertyData.dataType = SpecApiDataType.ListString.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(Int32)) || requestProp.PropertyType.Equals(typeof(Int32?)))
                {
                    propertyData.dataType = SpecApiDataType.Int.Value();
                    result.Add(propertyData);

                }
                else if (requestProp.PropertyType.Equals(typeof(Boolean)) || requestProp.PropertyType.Equals(typeof(Boolean?)))
                {
                    propertyData.dataType = SpecApiDataType.Boolean.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(Decimal)) || requestProp.PropertyType.Equals(typeof(Decimal?)))
                {
                    propertyData.dataType = SpecApiDataType.Decimal.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(List<int>)))
                {
                    propertyData.dataType = SpecApiDataType.ListInt.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(DateTime)) || requestProp.PropertyType.Equals(typeof(DateTime?)))
                {
                    propertyData.dataType = SpecApiDataType.DateTime.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.IsGenericType && (requestProp.PropertyType.GetGenericTypeDefinition() == typeof(List<>)))
                {
                    propertyData.dataType = SpecApiDataType.ListObject.Value();
                    propertyData.layer = requestProp.Name;
                    result.Add(propertyData);

                    Type requestTypeIn = Type.GetType(requestProp.PropertyType.FullName + "," + requestProp.PropertyType.Assembly.FullName);
                    IList<PropertyInfo> requestPropsS = new List<PropertyInfo>(requestTypeIn.GetProperties());
                    ObjectProperty objectProperty = new ObjectProperty();
                    objectProperty.assamblyName = requestPropsS[2].PropertyType.Assembly.FullName;
                    objectProperty.propertyTypeName = requestPropsS[2].PropertyType.FullName;
                    result.AddRange(GetChildrenType(this.apiSpectProperty.dataRow, objectProperty, propertyData.number));

                }
                else
                {
                    Type requestTypeItem;
                    if (requestProp.Name == "items")
                    {
                        requestTypeItem = this.itemData.GetType();
                        IList<PropertyInfo> requestPropsItems = new List<PropertyInfo>(requestTypeItem.GetProperties());
                        if (requestTypeItem.IsGenericType && (requestTypeItem.GetGenericTypeDefinition() == typeof(List<>)))
                        {
                            propertyData.dataType = SpecApiDataType.ListObject.Value();
                            propertyData.layer = requestProp.Name;
                            result.Add(propertyData);

                            ObjectProperty objectProperty = new ObjectProperty();
                            objectProperty.assamblyName = requestPropsItems[2].PropertyType.Assembly.FullName;
                            objectProperty.propertyTypeName = requestPropsItems[2].PropertyType.FullName;
                            result.AddRange(GetChildrenType(this.apiSpectProperty.dataRow, objectProperty, propertyData.number));
                        }
                        else
                        {
                            propertyData.dataType = SpecApiDataType.Object.Value();
                            propertyData.layer = requestProp.Name;
                            result.Add(propertyData);

                            ObjectProperty objectProperty = new ObjectProperty();
                            objectProperty.assamblyName = requestTypeItem.Assembly.FullName;
                            objectProperty.propertyTypeName = requestTypeItem.FullName;

                            result.AddRange(GetChildrenType(this.apiSpectProperty.dataRow, objectProperty, propertyData.number));
                        }

                    }
                    else
                    {

                        propertyData.dataType = SpecApiDataType.Object.Value();
                        propertyData.layer = requestProp.Name;
                        result.Add(propertyData);

                        ObjectProperty objectProperty = new ObjectProperty();
                        objectProperty.assamblyName = requestProp.PropertyType.Assembly.FullName;
                        objectProperty.propertyTypeName = requestProp.PropertyType.FullName;

                        result.AddRange(GetChildrenType(this.apiSpectProperty.dataRow, objectProperty, propertyData.number));
                    }
                }
                inputCollumnLast = sendType;
                runnungNumber++;
            }
            return result;
        }

        public List<DataTransferProperty> GetChildrenType(List<DataTransferProperty> comments, ObjectProperty inputRecursive, string runningNum)
        {
            List<DataTransferProperty> result = new List<DataTransferProperty>();
            Type requestType = Type.GetType(inputRecursive.propertyTypeName + "," + inputRecursive.assamblyName);
            IList<PropertyInfo> requestProps = new List<PropertyInfo>(requestType.GetProperties());
            int runningNumber = 1;

            foreach (var requestProp in requestProps)
            {
                DataTransferProperty propertyData = new DataTransferProperty();
                propertyData.mo = !string.IsNullOrWhiteSpace(GetAttributeProperty.GetPropertyTypeFromObjoct(requestType, requestProp.Name, ComponentModelType.Required)) ? RequireDataType.Mandatory.Value() : RequireDataType.Optional.Value();
                propertyData.description = GetAttributeProperty.GetPropertyTypeFromObjoct(requestType, requestProp.Name, ComponentModelType.Description);
                propertyData.fieldName = requestProp.Name;
                propertyData.number = runningNum + "." + runningNumber.ToString();
                propertyData.dataType = requestProp.PropertyType.Name;

                if (requestProp.PropertyType.Equals(typeof(string)))
                {
                    propertyData.dataType = SpecApiDataType.String.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(List<string>)))
                {
                    propertyData.dataType = SpecApiDataType.ListString.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(Int32)) || requestProp.PropertyType.Equals(typeof(Int32?)))
                {
                    propertyData.dataType = SpecApiDataType.Int.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(Boolean)) || requestProp.PropertyType.Equals(typeof(Boolean?)))
                {
                    propertyData.dataType = SpecApiDataType.Boolean.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(Decimal)) || requestProp.PropertyType.Equals(typeof(Decimal?)))
                {
                    propertyData.dataType = SpecApiDataType.Decimal.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(List<int>)))
                {
                    propertyData.dataType = SpecApiDataType.ListInt.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.Equals(typeof(DateTime)) || requestProp.PropertyType.Equals(typeof(DateTime?)))
                {
                    propertyData.dataType = SpecApiDataType.DateTime.Value();
                    result.Add(propertyData);
                }
                else if (requestProp.PropertyType.IsGenericType && (requestProp.PropertyType.GetGenericTypeDefinition() == typeof(List<>)))
                {
                    propertyData.dataType = SpecApiDataType.ListObject.Value();
                    propertyData.layer = requestProp.Name;
                    result.Add(propertyData);

                    Type requestTypeIn = Type.GetType(requestProp.PropertyType.FullName + "," + requestProp.PropertyType.Assembly.FullName);
                    IList<PropertyInfo> requestPropsS = new List<PropertyInfo>(requestTypeIn.GetProperties());

                    if (requestTypeIn != null)
                    {
                        ObjectProperty objectProperty = new ObjectProperty();
                        objectProperty.assamblyName = requestPropsS[2].PropertyType.Assembly.FullName;
                        objectProperty.propertyTypeName = requestPropsS[2].PropertyType.FullName;

                        result.AddRange(GetChildrenType(result, objectProperty, propertyData.number));
                    }
                }
                else
                {

                    if (requestProp.Name == "items")
                    {
                        Type requestTypeItem;

                        requestTypeItem = this.itemData.GetType();
                        IList<PropertyInfo> requestPropsItems = new List<PropertyInfo>(requestTypeItem.GetProperties());
                        if (requestTypeItem.IsGenericType && (requestTypeItem.GetGenericTypeDefinition() == typeof(List<>)))
                        {
                            propertyData.dataType = SpecApiDataType.ListObject.Value();
                            propertyData.layer = requestProp.Name;
                            result.Add(propertyData);

                            ObjectProperty objectProperty = new ObjectProperty();
                            objectProperty.assamblyName = requestPropsItems[2].PropertyType.Assembly.FullName;
                            objectProperty.propertyTypeName = requestPropsItems[2].PropertyType.FullName;

                            result.AddRange(GetChildrenType(this.apiSpectProperty.dataRow, objectProperty, propertyData.number));
                        }
                        else
                        {
                            propertyData.dataType = SpecApiDataType.Object.Value();
                            propertyData.layer = requestProp.Name;
                            result.Add(propertyData);

                            ObjectProperty objectProperty = new ObjectProperty();

                            objectProperty.assamblyName = requestTypeItem.Assembly.FullName;
                            objectProperty.propertyTypeName = requestTypeItem.FullName;

                            result.AddRange(GetChildrenType(this.apiSpectProperty.dataRow, objectProperty, propertyData.number));
                        }
                    }
                    else
                    {
                        Type requestTypeIn = Type.GetType(requestProp.PropertyType.FullName + "," + requestProp.PropertyType.Assembly.FullName);
                        if (requestTypeIn != null)
                        {
                            ObjectProperty objectProperty = new ObjectProperty();

                            objectProperty.assamblyName = requestProp.PropertyType.Assembly.FullName;
                            objectProperty.propertyTypeName = requestProp.PropertyType.FullName;

                            result.AddRange(GetChildrenType(result, objectProperty, propertyData.number));
                        }
                    }
                }


                runningNumber++;
            }

            return result;
        }

        public DataTransferProperty GetHeaderPropertySpect(string sendType)
        {
            DataTransferProperty result = new DataTransferProperty();
            result.input = sendType;
            result.dataType = "ComHeader";
            result.layer = "Https";
            result.description = "HTTPS header";
            result.number = "1";
            result.fieldName = "header";

            return result;
        }

        public List<DataTransferProperty> GetHeaderPropertySpect(string[] inputData)
        {
            List<DataTransferProperty> result = new List<DataTransferProperty>();
            int runnungNumber = 1;
            foreach (var requestProp in inputData)
            {
                DataTransferProperty dataTransferProperty = new DataTransferProperty();
                dataTransferProperty.dataType = "String";
                dataTransferProperty.number = runnungNumber.ToString();
                dataTransferProperty.fieldName = requestProp;

                result.Add(dataTransferProperty);
                runnungNumber++;
            }
            return result;
        }



    }

}
