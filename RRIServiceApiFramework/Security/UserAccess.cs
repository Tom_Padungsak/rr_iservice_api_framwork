﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRApiFramework
{
    public class UserAccess
    {
        public string email { get; set; }
        public string staffCode { get; set; }
        public string empNo { get; set; }
        public string userGroupCode { get; set; }
        public string userRoleCode { get; set; }
        public string userCode { get; set; }
        public string userTypeCode { get; set; }
        public List<string> userChildList { get; set; }
        public int userTypeId { get; set; }
        public string userName { get; set; }
        public string name { get; set; }
        public string isActive { get; set; }
        public long tokenDateTime { get; set; }
        public string isLogout { get; set; }
        public string userChannel { get; set; }
        public List<string> apiKeyList { get; set; }
    }
}