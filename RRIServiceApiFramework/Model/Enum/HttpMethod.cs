﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRApiFramework.Model.Enum
{
    public enum HttpMethod
    {
        [DefaultValue("GET")]
        Get = 0,

        [DefaultValue("POST")]
        Post = 1,

        [DefaultValue("PUT")]
        Put = 2,

        [DefaultValue("DELETE")]
        Delete = 4,

       
    }
}