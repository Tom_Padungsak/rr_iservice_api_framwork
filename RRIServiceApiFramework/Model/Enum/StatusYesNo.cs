﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRApiFramework.Model.Enum
{
    public enum StatusYesNo
    {
        [DefaultValue("Y")]
        Yes = 0,

        [DefaultValue("N")]
        No = 1,
    }
}